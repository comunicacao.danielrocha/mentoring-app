import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface UserState {
    username: string,
    userToken: string,
}

const initialState: UserState = {
    username: '',
    userToken: ''
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        logIn: (state, action: PayloadAction<string>) => {
            state.username = action.payload,
            state.userToken = Math.random().toString()
        },
    },
})

// Action creators are generated for each case reducer function
export const { logIn } = userSlice.actions

export default userSlice.reducer