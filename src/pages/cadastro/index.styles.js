import Image from 'next/image'
import styled from 'styled-components'
import Myimg from 'src/public/logo.png';
import { Formik, Form, Field } from 'formik';


export const MyBox = styled.div`
    width: 100%;
    height: 100vh;
    background-color: "#f1f1f1";
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 5%;
    /* justify-content: center; */
`

export const MyImage = styled(Image)`
    width: 200px;
    height: 200px;
`

export const MyInput = styled.input`
    width: 400px;
    height: 30px;
    margin-top: 40px;
`

export const MyFormik = styled(Formik)`
    flex-direction: column;
`

export const MyForm = styled(Form)`
    display: flex;
    flex-direction: column;
`

export const MyField = styled(Field)`
    width: 400px;
    height: 30px;
    margin-top: 30px;
`

export const MyButton = styled.button`
    background-color: transparent;
    border-width: 2;
    border-color: black;
    margin-top: 40px;
    border-radius: 4px;
    width: 400px;
    height: 30px;
    /* color: white; */
    &:hover {
        color: white;
        background-color: firebrick;
        border-width: 0;
        box-shadow: 0px 10px 20px rgba(82, 8,8,255);
        border-radius: 8px;
        transition: 0.3s ease-in-out;
        /* transform: translateX(-5px); */
    }
`

MyImage.defaultProps = {
    src: Myimg
}