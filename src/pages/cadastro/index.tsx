import { useRouter } from 'next/router'
import React from 'react'
import { useDispatch } from 'react-redux'
import { logIn } from '../../store/userSlice'
import { MyBox, MyButton, MyField, MyForm, MyFormik, MyImage } from './index.styles'

export default function Cadastro() {
    const router = useRouter()
    const dispatch = useDispatch()

    return (
        <MyBox>
            <MyImage />
            {/* parametros não aparecem aqui usando styled components */}
            <MyFormik
                initialValues={{
                    username: '',
                    password: '',
                }}
                onSubmit={async (values: any) => {
                    dispatch(logIn(values.username))
                    router.push('/welcome')
                }}
            >
                <MyForm>
                    <MyField required name='username' type='email' placeholder='Nome de usuario ou email' />
                    <MyField required name='password' type='password' placeholder='Sua melhor senha' />
                    <MyButton type='submit'>
                        Criar Conta
                    </MyButton>

                </MyForm>
            </MyFormik>
        </MyBox>
    )
}
