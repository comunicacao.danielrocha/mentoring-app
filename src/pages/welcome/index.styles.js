import Image from 'next/image'
import styled from 'styled-components'
import Myimg from 'src/public/logo.png';
import { Formik, Form, Field } from 'formik';


export const MyBox = styled.div`
    width: 100%;
    height: 100vh;
    background-color: "#f1f1f1";
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 5%;
    /* justify-content: center; */
`

export const MyImage = styled(Image)`
    width: 200px;
    height: 200px;
`

export const MyParagraph = styled.p`
    font-size: 28px;
    width: 400px;
    height: 30px;
    margin-top: 40px;
`

MyImage.defaultProps = {
    src: Myimg
}