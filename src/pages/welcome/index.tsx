import React from 'react'
import type { RootState } from '../../store'
import { useSelector} from 'react-redux'
import { MyBox, MyImage, MyParagraph } from './index.styles'

export default function Cadastro() {
    const username = useSelector((state: RootState) => state.user.username)

    return (
        <MyBox>
            <MyImage />
            <MyParagraph>
                Olá, {username}!
            </MyParagraph>
        </MyBox>
    )
}
